[TOC]

# Usage of this image
Use this image in your `bitbucket-pipelines.yml` like this:
```
image: simpletechs/pipeline-build:latest
```

In your steps, you have access to some handy env-variables as well. See here for details: https://confluence.atlassian.com/bitbucket/environment-variables-in-bitbucket-pipelines-794502608.html

# List of required env-variables
These env-variables have to be setup in bitbucket-pipelines, preferably on the account-level.

`PIPELINES_CACHE_GIT_REPO` - this is the repository where the cache will be saved to
`PIPELINES_GIT_EMAIL` - the email you want to be shown in git commits
`PIPELINES_GIT_USERNAME` - the username you want to be shown in git commits
`PIPELINES_SSH_KEY` - the hex(!) content of your private key `id_rsa` (see **Setting your private key**)

# Tools
All tools are located under `/tools/`. Each tool performs one function only.

## cmds
This script contains misc. tools that improve the experience with bitbuket pipelines.

These tools are currently supported:

Command   | Usage                         | Description
--------- |------------------------------ | -----------
success   | `success "Build finished"`    | send a success-message via hipchat (if configured)
fail      | `fail "Error while building"` | send a failure-message via hipchat (if configured)
git_setup | `git_setup`                   | setup git config

### Usage
Enable all cmds through sourcing the `cmds`-File in `bitbucket-pipelines.yml`:
```
       - source /tools/cmds
```

### env-variables
This tool also exports a number of handy env-variables, such as:

Environment variable     | Description
------------------------ | -----------
BITBUCKET_COMMIT_SHORT   | the short version of this commit's hash
BITBUCKET_PIPELINES_LINK | a link to this repository's pipelines on bitbucket
BITBUCKET_REPO_LINK      | a link to this repository and branch on bitbucket

## git-setup
This script sets up the ssh keys on the docker machine so that private git repos can be accessed.
**NOTE: After this tool ran, all commands will be able to identify as the owner of the private key. Make sure to restrict access accordingly!**

Set these ENV-Variables in bitbucket pipelines to make it work:

Environment variable   | Description
---------------------- | -----------
PIPELINES_SSH_KEY      | the content of your private key `id_rsa` (see below)
PIPELINES_GIT_EMAIL    | the email you want to be shown in git commits
PIPELINES_GIT_USERNAME | the username you want to be shown in git commits

### Setting your private key
Due to the way line-breaks are handled by bitbucket-pipelines, you need to do a bit of work with your private key.
Instead of setting the key directly, make sure to hexdump it like this:
```
xxd -p ~/.ssh/id_rsa | tr -d '\n'
```

Set the resulting String as `PIPELINES_SSH_KEY` in bitbucket-pipelines.
It must begin and end with `2d2d2d2d2d(0a)`.

**NOTE: Your private key must not have a passphrase set!**

## cache
This tool is meant to speed up deployment. It works by **caching** the dependencies of your app in a *seperate* git-repository.
Whenever the dependecy-file you have changes, it will re-generate the cache.

### Usage
```
    /tools/cache.sh -n "$BITBUCKET_REPO_SLUG/$BITBUCKET_BRANCH" -t yarn
```

If you want to run multiple _install_ commands at once (i.e. `bower` + `yarn`), just call the `cache.sh` twice with a different `-t` parameter.

### Caveats
Unfortunately, global modules are not supported. You can run those modules by prefixing them with `./node_modules/.bin/` - so instead of `grunt` you write `./node_modules/.bin/grunt`.

## hc

Send commands to hipchat. This is a local copy of https://github.com/hipchat/hipchat-cli - see the included Readme for full details.
Uses the following env-variables:

Environment variable | Description
-------------------- | -----------
HIPCHAT_TOKEN        | API token
HIPCHAT_ROOM_ID      | Room ID
HIPCHAT_FROM         | From name
HIPCHAT_COLOR        | Message color (yellow, red, green, purple or random - default: yellow)
HIPCHAT_FORMAT       | Message format (html or text - default: html)
HIPCHAT_NOTIFY       | Trigger notification for people in the room (default: 0)
HIPCHAT_HOST         | API host (api.hipchat.com)
HIPCHAT_LEVEL        | Message Level (targetting Nagios states, critical, warning, unknown, ok)
HIPCHAT_API          | API version (default: v1)

Usage:
```
/tools/hc -i "Message to send"
```
OR
```
echo "Message to send" | /tools/hc
```


# Sample bitbucket-pipelines.yml
```
image: simpletechs/pipeline-build:latest

pipelines:
  default:
    - step:
        script:
          - /tools/git-setup.sh
          - /tools/cache.sh -n "$BITBUCKET_REPO_SLUG/$BITBUCKET_BRANCH" -t yarn
          - /tools/cache.sh -n "$BITBUCKET_REPO_SLUG/$BITBUCKET_BRANCH" -t bower
          - ./node_modules/.bin/grunt deploy-dev
  branches:
    master:
      - step:
          script:
            - /tools/git-setup.sh
            - /tools/cache.sh -n "$BITBUCKET_REPO_SLUG/$BITBUCKET_BRANCH" -t yarn
            - /tools/cache.sh -n "$BITBUCKET_REPO_SLUG/$BITBUCKET_BRANCH" -t bower
            - ./node_modules/.bin/grunt deploy
```

# Developing a new tool / running locally
If you want to add a new tool and run the image locally, here's how you can build if from the Dockerfile:

First, create an `.env-local` file that conatins all your env-variables:
```
PIPELINES_CACHE_GIT_REPO=...
PIPELINES_GIT_EMAIL=...
PIPELINES_GIT_USERNAME=...
PIPELINES_SSH_KEY=...
```

Then build + run the image. Note that `--rm` instructs docker to delete the container after run.
```
$ docker build -t pipelines-build .
$ docker run --rm -ti --env-file .env-local pipelines-build /bin/bash

```