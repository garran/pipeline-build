#!/bin/bash

# This script sets up the ssh keys on the docker machine so that private git repos can be accessed.
# Set these ENV-Variables in bitbucket pipelines to make it work:
#
# SSH
#   PIPELINES_SSH_KEY
#   PIPELINES_SSH_KEY_PUB
# GIT
#   PIPELINES_GIT_EMAIL
#   PIPELINES_GIT_USERNAME

mkdir -p ~/.ssh
chmod 700 ~/.ssh/
# cat > ~/.ssh/id_rsa << KEY
# -----BEGIN RSA PRIVATE KEY-----
# $PIPELINES_SSH_KEY
# -----END RSA PRIVATE KEY-----
# KEY
# hex-unencode the ssh key so as to preserve any linebreaks
echo $PIPELINES_SSH_KEY | xxd -r -p - > ~/.ssh/id_rsa

# restrict access
chmod 400 ~/.ssh/id_rsa

# generate public key
ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub

# restrict access
chmod 400 ~/.ssh/id_rsa.pub


# setup git config
git config --global user.email $PIPELINES_GIT_EMAIL
git config --global user.name $PIPELINES_GIT_USERNAME

# add bitbucket and github public keys
echo "adding bitbucket and github public keys"
# ssh-keyscan github.com >> ~/.ssh/known_hosts
# ssh-keyscan -t rsa bitbucket.org >> ~/.ssh/known_hosts
cat <<EOF >> ~/.ssh/known_hosts
# github.com:22 SSH-2.0-libssh-0.7.0
github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
# bitbucket.org:22 SSH-2.0-conker_1.0.268-723a194 app-127
bitbucket.org ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
EOF