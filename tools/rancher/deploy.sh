#!/bin/bash

# run as $0 "stack/service-name" "image_with_tag" --options
export PLUGIN_URL=$RANCHER_URL
export PLUGIN_ACCESS_KEY=$RANCHER_KEY
export PLUGIN_SECRET_KEY=$RANCHER_SECRET

# todo: catch errors, run revoke regardless but exit after that
/tools/aws/authorize-current-ip.sh || true
drone-rancher --service "$1" --docker-image "$2" "${@:3}" || true
/tools/aws/authorize-current-ip.sh revoke || true