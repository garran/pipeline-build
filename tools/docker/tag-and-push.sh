#!/bin/bash
# run as: $0 "repo/image:tag" "tag" "[if set]"

IMAGE=$1
# replace / with _ in tags
NEW_TAG=${2//\//_}

if [ -z "$3" ]; then
    echo "condition not fulfilled to tag $IMAGE as $NEW_TAG"
    exit 0
fi

docker tag "$IMAGE" "$IMAGE:$NEW_TAG"
docker push "$IMAGE:$NEW_TAG"