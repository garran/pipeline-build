#!/bin/bash
# from http://stackoverflow.com/a/32918860/1635864

# Retrieve current IP address
IP=`curl -s http://whatismyip.akamai.com/`

if [ "x$1" == "x" ]; then
    echo "Authorizing $IP for access to $AWS_SECURITY_GROUP @ Port $AWS_SECURITY_GROUP_PORT"
    aws ec2 authorize-security-group-ingress --group-id $AWS_SECURITY_GROUP --protocol tcp --port $AWS_SECURITY_GROUP_PORT --cidr $IP/32 --output text || echo "Failed, but not cancelling (might have been set before)"
else
    echo "Revoking authorisation of $IP for access to $AWS_SECURITY_GROUP @ Port $AWS_SECURITY_GROUP_PORT"
    aws ec2 revoke-security-group-ingress --group-id $AWS_SECURITY_GROUP --protocol tcp --port $AWS_SECURITY_GROUP_PORT --cidr $IP/32 --output text
fi